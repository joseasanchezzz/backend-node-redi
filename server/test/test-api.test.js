const redis = require('redis');
const client = redis.createClient();
const assert = require('assert');
const request = require('supertest');
const app = require('../server');
const {_key_generator} = require('../services/service');
describe("Test api-node", () =>{

    let token ='';

  before(done=>{
            const email ='test@gmail.com';
            const password = '123456';
            let id = _key_generator({ 'id' : email+'-'+password})

            
            client.hmset(`${id}`,{
                "id" : id,
                "email": email,
                 "password": password },(err,result)=>{
                     if(err) console.error(err);
                        done();
                 })
        });
 


    describe("Register user in redis", ()=>{
        it("should respond 201 register",done=>{
            const email ='userTest@gmail.com';
            const password = 'test';
            
            request(app)
            .post("/users")
            .send({'email':email,'password':password})
            .end((err,result)=>{
                assert(result.body.ok === true);
                done();
            });
    });

    });
   

    describe('Login user test', () => {

        it("should respond  200 login" , done =>{
            request(app)
            .post('/login')
            .send({'email':'test@gmail.com','password':'123456'})
            .end((err,result)=>{
                token = result.body.token;
                assert(result.body.ok === true) 
                done();
            })
        });
    });
   


describe("User logged with token  test",()=>{

    it(' With token valid should respond 200 ', done=>{

          request(app)
          .get(`/home/${token}`)
          .end((err,result)=>{
                assert(result.body.ok === true);
              done();
          });

    });


    it(' With token invalid should respond 403 ', done=>{
        request(app)
        .get(`/home/sesion`)
        .end((err,result)=>{
              assert(result.body.ok === false && result.status === 403);
            done();
        });
    });





});





});