const express = require("express");
const redis = require('redis');
const https = require('https');
const app = express();

const client = redis.createClient();
const {_key_generator} = require('../services/service')


const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())


app.post("/", (req, res) => {



 res.status(200).json({
   ok:true,
   message: 'hola mundo',
   data:{
     'email': req.body.email,
     'password': req.body.pas
   }
 })
});


app.post("/users", (req, res) => {
  let body = req.body;
  const email = body.email;
  const password = body.password;
  let id = _key_generator({ 'id' : email+'-'+password})
  client.hmset(`${id}`,{
  "id" : id,
  "email": email,
   "password": password }
  , (err, reply)=> {

if(err) return res.json({ok: false, err})

res.status(201).json({ok: true, user: reply})
});
});

app.get("/home/:token", (req,res)=> {
  const token = req.params.token
  client.exists(token,(err,result)=>{
    if(result === 0){
      res.status(403).json({
        ok:false,
        token: 'no valid'
    
      });
    }else{
      let data = '';
      https.get('https://rickandmortyapi.com/api/character/', (resp) => {
      
        resp.on('data', (chunk) => {
          data += chunk;
        });
      
        // The whole response has been received. Print out the result.
        resp.on('end', async () => {
          data = JSON.parse(data).results

          let newData = [];
          data.map( data => {
            const { id,name,status,species,gender, image} = data;
              newData.push({id,name,status,species,gender,image})
          })
       await   res.json({
            ok:true,
            token: 'valid',
            data : newData
          });
        });
      
      }).on("error", (err) => {
        console.log("Error: " + err.message);
      });

    
    }

  });
});


module.exports = app