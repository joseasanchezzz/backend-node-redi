
const express = require("express");
const redis = require('redis');
const app = express();
const client = redis.createClient();
const {_key_generator} = require('../services/service');
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())


// app.post('/login', (req, res) => {
//  

// });

app.post('/login', (req,res)=>{
  let body = req.body;
    const email = body.email
    const password = body.password
  
    let id = _key_generator({ 'id' : email+'-'+password})
  
    client.exists(`${id}`, async  (err, replies) => {
   
  
     if(replies === 1){
          const token = `sesion-${id}`
      client.set(token,'sesion');
      client.expire(token,120);
  
      res.status(200).json({
        ok:true,
        user: 'encontrado',
        token
      })
     }else{
      res.status(404).json({
        ok:false,
        user: 'no encontrado',
      })
     }
    });
})

module.exports = app;