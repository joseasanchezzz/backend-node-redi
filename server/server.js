require('./config/config');
const express = require('express')
const redis = require('redis');

const app = express();


//creamos un cliente
const client = redis.createClient();
client.on('connect', ()=> {
    console.log('Conectado a Redis Server');});


const bodyParser = require("body-parser");
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
// parse application/json
app.use(bodyParser.json());

app.use(require("./routes/index"));

app.listen(process.env.PORT, ()=> {
    console.log(`escuchando el puerto ${process.env.PORT}`);
})

module.exports = app